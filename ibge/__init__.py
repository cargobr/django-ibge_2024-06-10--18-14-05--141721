VERSION = (0, 2)


def get_version():
    """Returns the version as a human-format string.
    """
    return '.'.join([str(i) for i in VERSION])


__author__ = 'See the file AUTHORS'
__license__ = 'GNU General Public License (GPL), Version 3'
__url__ = 'https://bitbucket.org/semente/django-ibge'
__version__ = get_version()
