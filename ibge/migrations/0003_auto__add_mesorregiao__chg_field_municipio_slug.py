# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Mesorregiao'
        db.create_table('ibge_mesorregiao', (
            ('codigo_ibge', self.gf('django.db.models.fields.PositiveIntegerField')(unique=True, primary_key=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('uf', self.gf('django.db.models.fields.CharField')(max_length=2)),
        ))
        db.send_create_signal('ibge', ['Mesorregiao'])

        # Changing field 'Municipio.slug'
        db.alter_column('ibge_municipio', 'slug', self.gf('django.db.models.fields.SlugField')(max_length=64))

        # Adding index on 'Municipio', fields ['slug']
        db.create_index('ibge_municipio', ['slug'])


    def backwards(self, orm):
        
        # Removing index on 'Municipio', fields ['slug']
        db.delete_index('ibge_municipio', ['slug'])

        # Deleting model 'Mesorregiao'
        db.delete_table('ibge_mesorregiao')

        # Changing field 'Municipio.slug'
        db.alter_column('ibge_municipio', 'slug', self.gf('django.db.models.fields.CharField')(max_length=64))


    models = {
        'ibge.mesorregiao': {
            'Meta': {'ordering': "('uf', 'nome')", 'object_name': 'Mesorregiao'},
            'codigo_ibge': ('django.db.models.fields.PositiveIntegerField', [], {'unique': 'True', 'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'uf': ('django.db.models.fields.CharField', [], {'max_length': '2'})
        },
        'ibge.municipio': {
            'Meta': {'ordering': "('nome', 'codigo_ibge')", 'object_name': 'Municipio'},
            'codigo_ibge': ('django.db.models.fields.PositiveIntegerField', [], {'unique': 'True', 'primary_key': 'True'}),
            'codigo_mesorregiao': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'codigo_microrregiao': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'is_capital': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_polo': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '8', 'blank': 'True'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '11', 'decimal_places': '8', 'blank': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'populacao': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '64', 'db_index': 'True'}),
            'uf': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['ibge.UnidadeFederativa']"})
        },
        'ibge.unidadefederativa': {
            'Meta': {'ordering': "('nome',)", 'object_name': 'UnidadeFederativa'},
            'codigo_ibge': ('django.db.models.fields.PositiveIntegerField', [], {'unique': 'True', 'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'populacao': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'regiao': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'sigla': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '2'})
        }
    }

    complete_apps = ['ibge']
