# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'UnidadeFederativa'
        db.create_table('ibge_unidadefederativa', (
            ('codigo_ibge', self.gf('django.db.models.fields.PositiveIntegerField')(unique=True, primary_key=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('sigla', self.gf('django.db.models.fields.CharField')(unique=True, max_length=2)),
            ('regiao', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('populacao', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal('ibge', ['UnidadeFederativa'])

        # Adding model 'Municipio'
        db.create_table('ibge_municipio', (
            ('codigo_ibge', self.gf('django.db.models.fields.PositiveIntegerField')(unique=True, primary_key=True)),
            ('codigo_mesorregiao', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('codigo_microrregiao', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('uf', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ibge.UnidadeFederativa'])),
            ('is_capital', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('populacao', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('is_polo', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('latitude', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=8, blank=True)),
            ('longitude', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=11, decimal_places=8, blank=True)),
        ))
        db.send_create_signal('ibge', ['Municipio'])


    def backwards(self, orm):
        # Deleting model 'UnidadeFederativa'
        db.delete_table('ibge_unidadefederativa')

        # Deleting model 'Municipio'
        db.delete_table('ibge_municipio')


    models = {
        'ibge.municipio': {
            'Meta': {'ordering': "('nome', 'codigo_ibge')", 'object_name': 'Municipio'},
            'codigo_ibge': ('django.db.models.fields.PositiveIntegerField', [], {'unique': 'True', 'primary_key': 'True'}),
            'codigo_mesorregiao': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'codigo_microrregiao': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'is_capital': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_polo': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '8', 'blank': 'True'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '11', 'decimal_places': '8', 'blank': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'populacao': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'uf': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['ibge.UnidadeFederativa']"})
        },
        'ibge.unidadefederativa': {
            'Meta': {'ordering': "('nome',)", 'object_name': 'UnidadeFederativa'},
            'codigo_ibge': ('django.db.models.fields.PositiveIntegerField', [], {'unique': 'True', 'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'populacao': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'regiao': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'sigla': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '2'})
        }
    }

    complete_apps = ['ibge']