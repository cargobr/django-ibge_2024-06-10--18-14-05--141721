#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011-2012 Guilherme Gondim
# Copyright (c) 2009 Interlegis
#
# This file is part of Django IBGE.
#
#   Django IBGE is free software under terms of the GNU General Public License
#   version 3 (GPLv3) as published by the Free Software Foundation.  See the
#   file README for copying conditions.
#

import codecs
import os
import sys

from setuptools import setup, find_packages


if 'publish' in sys.argv:
    os.system('python setup.py sdist upload')
    sys.exit()

read = lambda filepath: codecs.open(filepath, 'r', 'utf-8').read()


# Dynamically calculate the version based on ibge.VERSION.
version = __import__('ibge').get_version()

setup(
    name='django-ibge',
    version=version,
    description=('Django application to provide information from '
                 'Brazilian Institute of Geography and Statistics '
                 '(IBGE).'),
    long_description=read(os.path.join(os.path.dirname(__file__), 'README.rst')),
    keywords = 'django apps ibge',
    author='Guilherme Gondim',
    author_email='semente+django-ibge@taurinus.org',
    maintainer='Guilherme Gondim',
    maintainer_email='semente+django-ibge@taurinus.org',
    license='GNU General Public License (GPL), Version 3',
    url='https://bitbucket.org/semente/django-ibge/',
    download_url='https://bitbucket.org/semente/django-ibge/downloads/',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    zip_safe=False,
    include_package_data=True,
    packages=find_packages(),
    package_data={'ibge': ['fixtures/*.json']},
)
