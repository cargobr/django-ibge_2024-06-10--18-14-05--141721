===========
Django IBGE
===========

----

**Este repositório era versionado utilizando Mercurial (hg), porém o**
**Bitbucket deixou de suportar esse tipo de versionamento. Como esse**
**repositório ainda é usado pelo cargobr-corp "convertemos" ele para**
**git. Todo o histório do repositório foi mantido, porém para acessá-lo**
**é necessário ter instalado o mercurial.**

----

**Django IBGE** is a pluggable application for `Django Web Framework` that
provides information from Brazilian Institute of Geography and Statistics
(IBGE).

Project page:
    http://bitbucket.org/semente/django-ibge

Django Web Framework:
    http://www.djangoproject.com


Upgrading to version 0.2
========================

Django IBGE 0.2 adds a new field (slug) so you must update your database.

South users
-----------

Run the migrate exactly like that::

  python manage.py migrate ibge --fake 0001
  python manage.py migrate ibge

Note: you must fake the migration 0001 since South migrations was introduced in
the new version.

Non-South users
---------------

Open the database shell::

  python manage.py dbshell

and run the following SQL command::

  ALTER TABLE ibge_municipio ADD COLUMN slug VARCHAR(64);


Copying conditions
==================

Django IBGE is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation; either version 3 of the License, or (at your option) any
later version.

Django IBGE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; see the file COPYING. If not, see http://www.gnu.org/licenses/
